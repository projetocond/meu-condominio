const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const { ANALYZE } = process.env;

module.exports = {
  // distDir: '.next',
  webpack: (config, {dev}) => {
    // BundleAnalyzerPlugin
    if (ANALYZE) {
      console.log('Running Bundle Analyzer...');
      config.plugins.push(
        new BundleAnalyzerPlugin({
          analyzerMode: 'server',
          analyzerPort: 8888,
          openAnalyzer: true
        })
      );
    }

    if (dev) {
      return config;
    }

    // SWPrecacheWebpackPlugin
    config.plugins.push(
      new SWPrecacheWebpackPlugin({
        // minify: true,
        navigateFallback: '/',
        verbose: true,
        staticFileGlobsIgnorePatterns: [/\.next\//],
        runtimeCaching: [{
            handler: 'networkFirst',
            urlPattern: /^https?.*/
          },
          {
            handler: 'networkFirst',
            urlPattern: /\/_next\/.*/
          }
        ]
      })
    );

    return config;
  }
}