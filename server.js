// Core
const path = require('path');
const next = require('next');
const express = require('express');
// Env vars
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dir: './src', dev });
const handle = app.getRequestHandler();
const port = parseInt(process.env.PORT, 10) || 3000;

// Server configs
app.prepare().then(() => {
  const server = express();

  // server.use("/static", express.static(__dirname + "/../static", {
  //   maxAge: "365d"
  // }));

  server.get('/service-worker.js', (req, res) => {
    const filePath = path.join(app.distDir, 'service-worker.js');
    res.setHeader('Cache-Control', 'no-cache');
    res.sendFile(filePath);
  });

  server.get('/manifest.json', (req, res) => {
    const filePath = path.join(app.dir, 'static', 'manifest.json');
    res.setHeader('Cache-Control', 'no-cache');
    res.sendFile(filePath);
  });

  server.get('*', (req, res) => {
    return handle(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});