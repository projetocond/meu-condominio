/* eslint-disable jsx-a11y/anchor-is-valid */
import PropTypes from 'prop-types';
// Nextjs
import Link from 'next/link';
// Material UI
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
// Components
import Layout from '../components/layout';
import Header from '../components/header';

// Style
const styles = theme => ({
  root: {
    textAlign: 'center',
    paddingTop: theme.spacing.unit * 20,
  },
});

// Component
function Foods(props) {
  const { classes } = props;

  return (
    <Layout title="Foods :: No Ap">
      <Header title="Alimentos" />
      <div className={classes.root}>
        <Typography variant="display1" gutterBottom>
          Material-UI
        </Typography>
        <Typography variant="subheading" gutterBottom>
          about page
        </Typography>
        <Typography gutterBottom>
          <Link href="/products">
            <a>Go to the products page</a>
          </Link>
        </Typography>
        <Button variant="contained" color="primary">
          Do nothing button
        </Button>
      </div>
    </Layout>
  );
}

// Props
Foods.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Foods);