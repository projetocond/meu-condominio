// Next
import Link from 'next/link';
// Material UI
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
// Components
import Layout from '../components/layout';
import Header from '../components/header';

// Component
export default () => (
  <Layout title="Home :: No Ap">
    <Header title="No Ap" />
    <p>Hello world</p>
    <p>scoped!</p>
    <Link href="/foods">
      <a>Alimentos</a>
    </Link>
    <Link href="/products">
      <a>Produtos</a>
    </Link>
    <Link href="/services">
      <a>Serviços</a>
    </Link>
  </Layout>
);
