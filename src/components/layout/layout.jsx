import Head from 'next/head';

export default ({ children, title }) => (
  <React.Fragment>
    <Head><title>{title || 'No Ap'}</title></Head>
    {children}
  </React.Fragment>
);
