import PropTypes from 'prop-types';
// Partials
import Topbar from './partials/topbar';
import Drawer from './partials/drawer';

// Component
class Header extends React.Component {
  constructor() {
    super();
    this.state = { showDrawer: false };
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  toggleDrawer(open) {
    this.setState({ showDrawer: open });
  };

  render() {
    const { title } = this.props;
    return (
      <React.Fragment>
        <Topbar title={title} toggleDrawer={this.toggleDrawer} />
        <Drawer isOpen={this.state.showDrawer} toggle={this.toggleDrawer} />
      </React.Fragment>
    );
  };
}

// Props
Header.propTypes = {
  title: PropTypes.string
};

Header.defaultProps = {
  title: ''
};

export default Header;
