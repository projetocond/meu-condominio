import PropTypes from 'prop-types';
// Material UI
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
// Partial
import Menu from './menu';

// Component
function Drawer(props) {
  const { isOpen, toggle } = props;
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
  return (
    <SwipeableDrawer
      open={isOpen}
      onOpen={() => toggle(true)}
      onClose={() => toggle(false)}
      disableBackdropTransition={!iOS}
      disableDiscovery={iOS}
    >
      <Menu />
    </SwipeableDrawer>
  );
}

export default Drawer;