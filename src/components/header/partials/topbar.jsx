import PropTypes from 'prop-types';
// Material UI
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
// Icons
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';

// Styles
const styles = {
  flexGrow: {
    flexGrow: 1,
  },
  iconLeft: {
    marginLeft: -10,
    marginRight: 10,
  },
  iconRight: {
    // marginLeft: -10,
    marginRight: -10,
  },
};

// Component
const Topbar = (props) => {
  const { classes, title, toggleDrawer } = props;
  return (
    <React.Fragment>
      <AppBar position="sticky">
        <Toolbar>

          <IconButton
            className={classes.iconLeft}
            color="inherit"
            aria-label="Menu"
            onClick={() => toggleDrawer(true)}
          >
            <MenuIcon />
          </IconButton>

          <Typography variant="title" color="inherit" className={classes.flexGrow}>
            {title}
          </Typography>

          <IconButton
            className={classes.iconRight}
            color="inherit"
            aria-label="Menu"
            onClick={() => toggleDrawer(true)}
          >
            <SearchIcon />
          </IconButton>

          {/* <Button color="inherit">Login</Button> */}
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
};

// Props
Topbar.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string
};

Topbar.defaultProps = {
  title: ''
};

export default withStyles(styles)(Topbar);