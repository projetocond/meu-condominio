import PropTypes from 'prop-types';
// Next
import Link from 'next/link';
// Material UI
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { withStyles } from '@material-ui/core/styles';
// Icons
import ServiceIcon from '@material-ui/icons/Build';
import ProductsIcon from '@material-ui/icons/CardGiftcard';
import FoodIcon from '@material-ui/icons/Fastfood';
import FavoriteIcon from '@material-ui/icons/Star';

// Styles
const styles = theme => ({
  content: {
    width: 260,
    backgroundColor: theme.palette.background.paper,
  },
});

// Component
const Menu = (props) => {
  const { classes } = props;

  return (
    <div className={classes.content}>
      <List component="nav">
        <Link href="/foods">
          <a>
            <ListItem button>
              <ListItemIcon>
                <FoodIcon />
              </ListItemIcon>
              <ListItemText primary="Alimentos" />
            </ListItem>
          </a>
        </Link>

        <Link href="/services">
          <a>
            <ListItem button>
              <ListItemIcon>
                <ServiceIcon />
              </ListItemIcon>
              <ListItemText primary="Serviços" />
            </ListItem>
          </a>
        </Link>

        <Link href="/products">
          <a>
            <ListItem button>
              <ListItemIcon>
                <ProductsIcon />
              </ListItemIcon>
              <ListItemText primary="Produtos" />
            </ListItem>
          </a>
        </Link>

        <Link href="/">
          <a>
            <ListItem button>
              <ListItemIcon>
                <FavoriteIcon />
              </ListItemIcon>
              <ListItemText primary="Favoritos" />
            </ListItem>
          </a>
        </Link>
      </List>

      <Divider />

      <List component="nav">
        <ListItem button>
          <ListItemText primary="Trash" />
        </ListItem>
        <ListItem button component="a" href="#simple-list">
          <ListItemText primary="Spam" />
        </ListItem>
      </List>
    </div>
  );
};

// Props
Menu.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Menu);
